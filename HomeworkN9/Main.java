package HomeworkN9;

public class Main {
    public static void main(String[] args) {

        BaseElement base = new BaseElement();
        LinkElement link = new LinkElement();
        TextElement text = new TextElement();

        base.getElement();
        base.click();

        link.getElement();
        link.click();

        text.getElement();
        text.click();

    }
}
/* I did the HW9 but I honestly still dont undertand this:
 4.Declare a string variable locator with a value "//*[id='some-id']",
 so it is in both classes (without duplicating the code)

 Please show me how it should be done, I'm still confused on that part.
 Hope it clicks with your explanation! thanks!
 */