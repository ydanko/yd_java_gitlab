public class homework_8 {

    public static void main(String[] args) {

//        - Finish up the logic that calculates the balance
//        - Create a method that withdraws from the balance
//        - Check that if a bank customer deposits $500, $150 and $35 and then withdraws $40 and $120 the balance is correct
//        - Print out a meaningful message if it is correct and if it is not

        banking_account myAccount = new banking_account();
        myAccount.deposit(500);
        myAccount.deposit(150);
        myAccount.deposit(35);

        if (myAccount.balance != 685){
            System.out.println("Deposit amount is incorrect");
        } else{
            System.out.println("Current balance after deposit is " + myAccount.balance);
        }
            myAccount.withdrawal(40);
        myAccount.withdrawal(120);

        if (myAccount.balance != 525){
            System.out.println("Withdrawal amount is incorrect");
        } else{
            System.out.println("Current balance after withdrawal is " + myAccount.balance);
        }

        // please finish the rest
    }
}


